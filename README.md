# Ubuntu 18.04 [![build status](https://gitlab.com/nvidia/container-images/cuda-ppc64le/badges/ubuntu18.04/build.svg)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/commits/ubuntu18.04)

## CUDA 10.2

- [`10.2-base`, `10.2-base-ubuntu18.04` (*10.2/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.2/base/Dockerfile)
- [`10.2-runtime`,`10.2-runtime-ubuntu18.04` (*10.2/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.2/runtime/Dockerfile)
- [`latest`, `10.2-devel`, `10.2-devel-ubuntu18.04` (*10.2/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/container-images/cuda-ppc64le/blob/ubuntu18.04/10.2/devel/Dockerfile)
- [`10.2-cudnn7-runtime`, `10.2-cudnn7-runtime-ubuntu18.04` (*10.2/runtime/cudnn7/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.2/runtime/cudnn7/Dockerfile)
- [`10.2-cudnn7-devel`, `10.2-cudnn7-devel-ubuntu18.04` (*10.2/devel/cudnn7/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.2/devel/cudnn7/Dockerfile)

## CUDA 10.1 update 2 (requires nvidia-docker v2)

- [`10.1-base`, `10.1-base-ubuntu18.04` (*10.1/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.1/base/Dockerfile)
- [`10.1-runtime`,`10.1-runtime-ubuntu18.04` (*10.1/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.1/runtime/Dockerfile)
- [`10.1-devel`, `10.1-devel-ubuntu18.04` (*10.1/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/container-images/cuda-ppc64le/blob/ubuntu18.04/10.1/devel/Dockerfile)
- [`10.1-cudnn7-runtime`, `10.1-cudnn7-runtime-ubuntu18.04` (*10.1/runtime/cudnn7/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.1/runtime/cudnn7/Dockerfile)
- [`10.1-cudnn7-devel`, `10.1-cudnn7-devel-ubuntu18.04` (*10.1/devel/cudnn7/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.1/devel/cudnn7/Dockerfile)

## CUDA 10.0 (requires nvidia-docker v2)

- [`10.0-base`, `10.0-base-ubuntu18.04` (*10.0/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.0/base/Dockerfile)
- [`10.0-runtime`,`10.0-runtime-ubuntu18.04` (*10.0/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.0/runtime/Dockerfile)
- [`latest`, `10.0-devel`, `10.0-devel-ubuntu18.04` (*10.0/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.0/devel/Dockerfile)
- [`10.0-cudnn7-runtime`, `10.0-cudnn7-runtime-ubuntu18.04` (*10.0/runtime/cudnn7/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.0/runtime/cudnn7/Dockerfile)
- [`10.0-cudnn7-devel`, `10.0-cudnn7-devel-ubuntu18.04` (*10.0/devel/cudnn7/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda-ppc64le/blob/ubuntu18.04/10.0/devel/cudnn7/Dockerfile)
